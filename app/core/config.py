from pydantic.class_validators import validator
from pydantic import BaseSettings, PostgresDsn
from typing import Any, Dict, Optional
import secrets
import dotenv
dotenv.load_dotenv()


class Settings(BaseSettings):
    API_V1: str = '/api/v1'
    SECRET_KEY: str = secrets.token_urlsafe(32)

    SQLALCHEMY_DATABASE_URI: str
    

    PROJECT_NAME: str

    # @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    # def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
    #     if isinstance(v, str):
    #         return v
    #     return PostgresDsn.build(
    #         scheme="postgresql",
    #         user=values.get("POSTGRES_USER"),
    #         password=values.get("POSTGRES_PASSWORD"),
    #         host=values.get("POSTGRES_SERVER"),
    #         path=f"/{values.get('POSTGRES_DB') or ''}",
    #     )

settings = Settings()
