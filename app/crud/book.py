from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from .base import CRUDBase
from models.book import Book
from schemas.book import BookBase, BookDisplay


class CRUDBook(CRUDBase[Book, BookBase]):
    def create(self, db: Session, *, obj_in: BookBase) -> Book:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data)
        db.add(db_obj)
        db.commit()

        db.refresh(db_obj)
        return db_obj

    def get_multi_books(self, db: Session, *, skip: int = 0, limit: int = 100) -> List[Book]:
        return db.query(self.model).offset(skip).limit(limit).all()

    def get_by_id(self, db: Session, id: int) -> Optional[Book]:
        return db.query(Book).filter(Book.id == id).first()


book = CRUDBook(Book)
