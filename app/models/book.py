from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationships

from db.session import Base


class Book(Base):
    __tablename__ = "book"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String, index=True)
    author = Column(String, index=True)
    category = Column(String, index=True)