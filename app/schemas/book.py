from typing import Optional

from pydantic import BaseModel

class BookBase(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None
    author: Optional[str] = None
    category: Optional[str] = None

class BookCreate(BookBase):
    title: str
    author: str

class BookDisplay(BaseModel):
    title: Optional[str] = None
    description: Optional[str] = None
    author: Optional[str] = None
    category: Optional[str] = None

    class Config:
        orm_mode = True