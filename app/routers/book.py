from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

from schemas.book import BookDisplay, BookCreate

from crud.book import book
from api.deps import get_db

router = APIRouter()


@router.post("/", response_model=BookDisplay)
def create_book(*, db: Session = Depends(get_db), book_in: BookCreate) -> Any:
    item = book.create(db=db, obj_in=book_in)
    return item


@router.get("/", response_model=List[BookDisplay])
def read_books(db: Session = Depends(get_db), skip: int = 0, limit: int = 100) -> Any:
    books = book.get_multi_books(db, skip=skip, limit=limit)
    return books


@router.get("/{book_id}", response_model=BookDisplay)
def read_book_by_id(book_id: int, db: Session = Depends(get_db)) -> Any:
    book_ = book.get_by_id(db, book_id)

    if not book_:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="The book not found"
        )

    return book_
